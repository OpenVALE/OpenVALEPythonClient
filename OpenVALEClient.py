"""
CLIENT API FOR OPENVALE

FUNCTIONS MISSING INCLUDE:
-->getCurrentHRTF
-->getSubjectNumber
"""

# Client package to interact with OpenVALE server
import socket
from enum import Enum
from time import sleep
from typing import overload


class NoiseType(Enum):
    wav = 1
    noise = 2
    asio = 3


class LocalizationResponse:
    def __init__(self, speakerID, responseTime):
        self.speakerID = speakerID
        self.responseTime = responseTime


class OpenVALEClient:
    def __init__(self, ipAddress, port, verbose=False):
        self.verbose = verbose

        if self.verbose:
            print("Connecting to server")

        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((ipAddress, port))
            print("Connected")
        except ConnectionRefusedError:
            print("Open OpenVALE!")
            exit(1)

        if self.verbose:
            print("OpenVALE Server Setup Complete")

    def disconnect(self):
        self.sock.close()

    def loadHRTF(self, filename):
        if self.verbose:
            print("Calling loadHRTF")

        message = "loadHRTF( " + filename + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to parse integer from " + reply.split(",")[1])
            return -1

        if self.verbose:
            print("Completed loadHRTF adding " + filename + "," + reply.split(",")[1])

        return parsedInteger

    def setHRTF(self, sourceNo, hrtfID):
        if self.verbose:
            print("Calling setHRTF")

        message = "setHRTF( " + str(sourceNo) + "," + str(hrtfID) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to parse integer from " + reply.split(",")[1])
            return -1

        if self.verbose:
            print("Completed setHRTF with ID " + str(hrtfID) + "," + reply.split(",")[1])

        return parsedInteger

    def addAudioSource(self, noiseType, location, id, file=""):
        if self.verbose:
            print("Calling addAudioSource")

        message = "addAudioSource("
        if noiseType == NoiseType.noise:
            message += "noiseGen," + (str(location) + "," + str(id) + ")\n")
        elif noiseType == NoiseType.wav:
            message += "wav," + str(location) + "," + str(id) + "," + file + ")\n"
        elif noiseType == NoiseType.asio:
            message += "asio," + str(location) + "," + str(id) + ",6)\n"
        self.sock.send(message.encode())
        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())
        try:
            parsedInteger = int(reply.split(",")[2])
        except TypeError:
            print("Failed to parse integer from " + reply.split(",")[2])
            return -1

        if self.verbose:
            if noiseType == NoiseType.noise:
                print("Completed AddAudioSource with Noise Source")
            elif noiseType == NoiseType.wav:
                print("Completed AddAudioSource from filename: " + file)

        return parsedInteger

    def enableSrc(self, sourceID, enable):
        if self.verbose:
            print("Calling enableSrc")

        message = "enableSrc(" + str(sourceID) + ","
        if enable:
            message += "T)\n"
        else:
            message += "F)\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to enable source " + str(sourceID))
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Completed enableSrc " + str(sourceID))
            else:
                print("Failed to complete enableSrc" + reply)

        return parsedInteger

    def startRendering(self):
        if self.verbose:
            print("Calling StartRendering")

        message = "StartRendering()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to start rendering")
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Started rendering")
            else:
                print("Failed to start rendering " + reply)

        return parsedInteger

    def endRendering(self):
        if self.verbose:
            print("Calling EndRendering")

        message = "EndRendering()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to end rendering")
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Ended rendering")
            else:
                print("Failed to end rendering " + reply)

        return parsedInteger

    def reset(self):
        if self.verbose:
            print("Calling Reset")

        message = "Reset()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to complete reset")
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Reset complete")
            else:
                print("Failed to reset " + reply)

        return parsedInteger

    def defineFront(self):
        if self.verbose:
            print("Calling defineFront")

        message = "defineFront()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to define front")
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Defined front")
            else:
                print("Failed to define front " + reply)

        return parsedInteger

    def adjustSourceLevel(self, sourceID, relativeLevel):
        if self.verbose:
            print("Calling adjustSourceLevel")

        message = "adjustSourceLevel(" + str(sourceID) + "," + str(relativeLevel) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())
        print(reply)
        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to adjust source level")
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Adjusted source level: " + str(sourceID) + "," + str(relativeLevel))
            else:
                print("Failed to adjust source level: " + str(sourceID) + "," + str(relativeLevel))

        return parsedInteger

    def adjustOverallLevel(self, relativeLevel):
        if self.verbose:
            print("Calling adjustOverallLevel")

        message = "adjustOverallLevel(" + str(relativeLevel) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to adjust overall level: " + str(relativeLevel))
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Adjusted overall level: " + str(relativeLevel))
            else:
                print("Failed to adjust overall level: " + str(relativeLevel))

        return parsedInteger

    def adjustSourcePosition(self, sourceID, location):
        if self.verbose:
            print("Calling adjustSourcePosition")

        message = "adjustSourcePosition(" + str(sourceID) + "," + str(location) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to adjust source position")
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Adjusted source position: " + str(location))
            else:
                print("Failed to adjust source position: " + str(location))

        return parsedInteger

    def adjustSourcePosition(self, sourceID, speakerID):
        if self.verbose:
            print("Calling adjustSourcePosition")

        message = "adjustSourcePosition(" + str(sourceID) + "," + str(speakerID) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to adjust source position: " + str(sourceID) + "," + str(speakerID))
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Adjusted source position: " + str(sourceID) + "," + str(speakerID))
            else:
                print("Failed to adjust source position: " + str(sourceID) + "," + str(speakerID))

        return parsedInteger

    def muteAudioSource(self, sourceID, mute):
        if self.verbose:
            print("Calling muteAudioSource")

        message = "muteAudioSource(" + str(sourceID) + ","
        if mute:
            message += "T)\n"
        else:
            message += "F)\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to mute source: " + reply)
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Muted audio source: " + reply)
            else:
                print("Failed to mute source: " + reply)

        return parsedInteger

    def setLEDs(self, sourceID, mask):
        if self.verbose:
            print("Calling setLEDs")

        message = "setLEDs(" + str(sourceID) + "," + str(mask) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed setLEDs: " + str(sourceID) + "," + str(mask))
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Set LEDs: " + str(sourceID) + "," + str(mask))
            else:
                print("Failed setLEDs: " + str(sourceID) + "," + str(mask))

        return parsedInteger

    def setLEDs(self, location, mask):
        if self.verbose:
            print("Calling setLEDs")

        message = "setLEDs(" + str(location) + "," + str(mask) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed setLEDs: " + str(location) + "," + str(mask))
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Set LEDs: " + str(location) + "," + str(mask))
            else:
                print("Failed setLEDs: " + str(location) + "," + str(mask))

        return parsedInteger

    def showFreeCursor(self, type, enable):
        if self.verbose:
            print("Calling showFreeCursor")

        message = "showFreeCursor(" + type + ","
        if enable:
            message += "T)\n"
        else:
            message += "F)\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to show free cursor: " + type)
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Completed show free cursor: " + type)
            else:
                print("Failed to show free cursor: " + type)

        return parsedInteger

    def showSnappedCursor(self, type, enable):
        if self.verbose:
            print("Calling showSnappedCursor")

        message = "showSnappedCursor(" + type + ","
        if enable:
            message += "T)\n"
        else:
            message += "F)\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed to show snapped cursor: " + type)
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Completed show snapped cursor: " + type)
            else:
                print("Failed to show snapped cursor: " + type)

        return parsedInteger

    def waitForResponse(self):
        if self.verbose:
            print("Calling waitForResponse")

        message = "waitForResponse()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            returnedID = int(reply.split(",")[2].replace("\\[", ""))
            responseTime = float(reply.split(",")[3].replace("]", ""))
        except TypeError:
            print("Failed to parse response from: " + reply)
            return -1

        response = LocalizationResponse(returnedID, responseTime)

        if self.verbose:
            print("Localization Response: " + str(response.speakerID) + " , " + str(response.responseTime))

        return response

    def waitForResponseAB(self):
        if self.verbose:
            print("Calling waitForResponse")

        message = "waitForResponseAB()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(64)
        reply = str(replyBytes.decode())

        print(reply)
        try:
            returnedID = reply.split(",")[2].replace("\\[", "")
            responseTime = float(reply.split(",")[3].replace("]", ""))
        except TypeError:
            print("Failed to parse response from: " + reply)
            return -1

        response = LocalizationResponse(returnedID, responseTime)

        if self.verbose:
            print("AB Response: " + str(response.speakerID) + " , " + str(response.responseTime))

        return response

    def waitForRecenter(self, speakerID, tolerance):
        if self.verbose:
            print("Calling waitForRecenter")

        message = "waitForRecenter(" + str(speakerID) + "," + str(tolerance) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed wait for recenter")
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Completed wait for recenter")
            else:
                print("Failed wait for recenter")

        return parsedInteger

    def waitForRecenter(self, location, tolerance):
        if self.verbose:
            print("Calling waitForRecenter")

        message = "waitForRecenter(" + str(location) + "," + str(tolerance) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed wait for recenter")
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Completed wait for recenter")
            else:
                print("Failed wait for recenter")

        return parsedInteger

    def getHeadOrientation(self):
        if self.verbose:
            print("Calling getHeadOrientation")

        message = "getHeadOrientation()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(64)
        reply = str(replyBytes.decode())

        try:
            xCoord = float(reply.split(",")[2].replace("[", ""))
            yCoord = float(reply.split(",")[3])
            zCoord = float(reply.split(",")[4].replace("]", ""))
        except (TypeError, ValueError):
            print("Failed to parse head orientation from: " + reply)
            return -1

        headOrientation = [xCoord, yCoord, zCoord]

        if self.verbose:
            print("Head Orientation: " + str(headOrientation))

        return headOrientation

    def getHead6DOF(self):
        if self.verbose:
            print("Calling getHead6DOF")

        message = "GetHead6DOF()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(128)
        reply = str(replyBytes.decode())

        try:
            xPos = float(reply.split(",")[2].replace("[", ""))
            yPos = float(reply.split(",")[3])
            zPos = float(reply.split(",")[4].replace("]", ""))
            xOri = float(reply.split(",")[5].replace("[", ""))
            yOri = float(reply.split(",")[6])
            zOri = float(reply.split(",")[7].replace("]", ""))
        except (TypeError, ValueError):
            print("Failed to parse head 6DOF from: " + reply)
            return -1

        head6DOF = [xPos, yPos, zPos, xOri, yOri, zOri]

        if self.verbose:
            print("Head 6DOF: " + str(head6DOF))

        return head6DOF

    def getNearestSpeaker(self, location):
        if self.verbose:
            print("Calling getNearestSpeaker")

        message = "getNearestSpeaker(" + str(location) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
            speakerNumber = int(reply.split(",")[2])
        except (TypeError, ValueError):
            print("Failed to get nearest speaker to: " + str(location))
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Completed get nearest speaker: " + str(parsedInteger))
            else:
                print("Failed to get nearest speaker to: " + str(location))

        return speakerNumber

    def getSpeakerPosition(self, speakerID):
        if self.verbose:
            print("Calling getSpeakerPosition")

        message = "getSpeakerPosition(" + str(speakerID) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(64)
        reply = str(replyBytes.decode())

        try:
            xCoord = float(reply.split(",")[2].replace("[", ""))
            yCoord = float(reply.split(",")[3])
            zCoord = float(reply.split(",")[4].replace("]", ""))
        except (TypeError, ValueError):
            print("Failed to parse speaker position from: " + reply)
            return -1

        headOrientation = [xCoord, yCoord, zCoord]

        if self.verbose:
            print("Speaker Position: " + str(headOrientation))

        return headOrientation

    def highlightLocation(self, speakerID=None, color=None):
        if self.verbose:
            print("Calling highlightLocation")

        if speakerID == None or color == None:
            message = "highlightLocation()\n"
        else:
            message = "highLightLocation(" + str(speakerID) + "," + str(color) + ")\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(64)
        reply = str(replyBytes.decode())

        try:
            print(reply)
            parsedInteger = int(reply.split(",")[1])
        except (TypeError, ValueError):
            print("Failed to highlight location: " + str(speakerID) + "," + str(color))
            return -1

        if parsedInteger == 0:
            print("Highlight Location: " + str(speakerID) + "," + str(color))
        else:
            print("Failed to highlight location: " + str(speakerID) + "," + str(color))

        return parsedInteger

    def displayMessage(self, displayMessage):
        if self.verbose:
            print("Calling displayMessage")

        print("MESSAGE: " + displayMessage)

        message = "displayMessage(" + displayMessage + ")\n"
        self.sock.send(message.encode())

        reply = ""
        while reply == "":
            replyBytes = self.sock.recv(32)
            reply = str(replyBytes.decode())

        try:
            parsedInteger = int(reply.split(",")[1])
        except TypeError:
            print("Failed display message: " + displayMessage)
            return -1

        if self.verbose:
            if parsedInteger == 0:
                print("Displayed message: " + displayMessage)
            else:
                print("Failed display message: " + displayMessage)

        return parsedInteger

    def getSubjectNumber(self):
        message = "getSubjectNumber()\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())
        try:
            parsedInteger = int(reply.split(",")[2])
        except TypeError:
            print("Failed getSubjectNumber")
            return -1
        return parsedInteger

    def sendNonStandardMessage(self, message):
        message += "\n"
        self.sock.send(message.encode())

        replyBytes = self.sock.recv(32)
        reply = str(replyBytes.decode())

        return reply
