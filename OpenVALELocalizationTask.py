from OpenVALEClient import *
from math import pi
from random import randint

'''
Localization task for OpenVALE
'''

def main():
    #####################
    #--SET UP OPENVALE--#
    #####################

    client = OpenVALEClient("localhost", 43201, True)
    hrtfNo = client.loadHRTF("GoldenClusterMean_Snow_SH6E150.slh")
    sourceID = client.addAudioSource(NoiseType.noise, [2.33, 0, 0], hrtfNo)
    client.startRendering()

    #Enable audio
    client.enableSrc(sourceID, True)
    client.muteAudioSource(sourceID, True)

    #Have user begin the script
    client.displayMessage("--FIRE TO BEGIN--")
    client.waitForResponse()
    client.displayMessage("")
    client.defineFront()

    #############################
    #--BEGIN LOCALIZATION TASK--#
    #############################

    i = 0
    while i < 10:
        #Enable snapped cursor with head
        client.showSnappedCursor("head", True)

        #Enable center highlight
        client.highlightLocation(274, [0.0, 1.0, 0.0])

        #Get user to look at center
        client.displayMessage("Move head so pointer is focused on highlighted speaker")
        client.waitForRecenter(274, 5*pi/180)

        #Disable highlight
        client.highlightLocation()

        #Turn off the cursor
        client.showSnappedCursor("head", False)

        #Generate a valid speaker location
        badFound = True
        badSpeakers = [60, 183, 208, 61, 252, 177, 37, 258, 202, 227, 36, 59, 62, 233]

        while badFound:
            randSpk = randint(1, 277)
            for speaker in badSpeakers:
                if randSpk == speaker:
                    badFound = True
                    break

                badFound = False

        #Move source to the random spk position
        client.adjustSourcePosition(sourceID, client.getSpeakerPosition(randSpk))

        #Get head position before playing sound
        beforeHeadPos = client.getHead6DOF()

        #Play sound for bursted duration
        client.muteAudioSource(sourceID, False)

        burstDuration = 0.25
        sleep(burstDuration)

        client.muteAudioSource(sourceID, True)

        #Get response
        client.displayMessage("Select the source of the sound")
        client.showSnappedCursor("hand", True)
        response = client.waitForResponse()

        #Enable highlight where the user clicked
        client.highlightLocation(response.speakerID, [1, 1, 1])
        client.showSnappedCursor("hand", False)
        sleep(0.5)

        #Give feedback on the user's selection
        if randSpk == response.speakerID:
            #Green highlight if correct
            client.highlightLocation(randSpk, [0, 1, 0])
        else:
            #Red highlight if wrong
            client.highlightLocation(randSpk, [1, 0, 0])

        #Make user acknowledge feedback
        client.displayMessage("Fire to continue")
        client.waitForResponse()

        #Turn off highlights
        client.highlightLocation()

        #Increment the loop counter
        i += 1

    client.reset()

main()