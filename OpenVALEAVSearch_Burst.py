from OpenVALEClient import *
from math import pi
from time import sleep
import random
import os

'''
Localization task for OpenVALE
'''


class AV_Trial_Data:
    def __init__(self):
        self.db_level = 0
        self.response_time = 0
        self.correct_response = 0
        self.actual_response = 0


def main():
    data_collection_folder = "./data/"
    possible_gain_levels = [0, 20, 40]
    #####################
    #--SET UP OPENVALE--#
    #####################

    client = OpenVALEClient("localhost", 43201, True)
    hrtfNo = client.loadHRTF("GoldenClusterMean_SH6E100_HD280.slh")
    sourceIDs = list()
    sourceIDs.append(client.addAudioSource(NoiseType.noise, [0, 0, 0], hrtfNo))

    client.startRendering()

    #Enable audio
    for source_id in sourceIDs:
        client.enableSrc(source_id, True)
        client.muteAudioSource(source_id, True)

    # Have user enter their subject number
    # client.showFreeCursor(True)
    client.displayMessage("Pull trigger when ready")
    client.waitForResponse()
    client.displayMessage("")
    client.defineFront()
    subject_number = client.getSubjectNumber()

    bad_speakers = [60, 183, 208, 61, 252, 177, 37, 258, 202, 227, 36, 59, 62, 233]
    speakers_possible = list(range(1, 277))

    for bad in bad_speakers:
        if bad in speakers_possible:
            speakers_possible.remove(bad)

    number_of_trials = 5
    used_speakers = list()
    trials = list()

    for trial_number in range(number_of_trials):
        trial_data = AV_Trial_Data()
        #Enable snapped cursor with head
        client.showSnappedCursor("head", True)

        #Enable center highlight
        client.highlightLocation(274, [0.0, 1.0, 0.0])

        #Get user to look at center
        client.displayMessage("Move head so pointer is focused on highlighted speaker")
        client.waitForRecenter(274, 5*pi/180)

        #Disable highlight
        client.displayMessage("")
        client.highlightLocation()

        #Turn off the cursor
        client.showSnappedCursor("head", False)
        even_speaker = random.choice(speakers_possible)
        distractors_count = 50
        odd_leds_count = 0
        used_speakers.append(even_speaker)
        while True:
            odd_speaker = random.choice(speakers_possible)
            if odd_speaker == even_speaker:
                continue
            odd_mask = random.choice(['1,0,0,0', '0,1,0,0', '0,0,1,0', '0,0,0,1', '0,1,1,1', '1,0,1,1', '1,1,0,1',
                                      '1,1,1,0'])
            client.setLEDs(odd_speaker, odd_mask)
            odd_leds_count += 1
            used_speakers.append(odd_speaker)
            if odd_leds_count >= distractors_count:
                break
        even_mask = random.choice(
            ['1,1,0,0', '1,0,1,0', '1,0,0,1', '0,1,1,0', '0,1,0,1', '0,0,1,1', '1,1,1,1', '1,1,1,1', '1,1,1,1',
             '1,1,1,1', '1,1,1,1', '1,1,1,1'])
        client.setLEDs(even_speaker, even_mask)

        #Move source to the random spk position
        source_id = random.choice(sourceIDs)
        client.adjustSourcePosition(source_id, client.getSpeakerPosition( even_speaker))

        #Play sound for bursted duration
        client.muteAudioSource(source_id, False)
        sleep(.25)
        client.muteAudioSource(source_id, True)
        # Get response
        response = client.waitForResponseAB()
        client.muteAudioSource(source_id, True)
        
        trial_data.correct_response = even_mask.count('1')
        
        trial_data.response_time = response.responseTime
        if response.speakerID == "a":
            trial_data.actual_response = 2
        elif response.speakerID == "b":
            trial_data.actual_response = 4
        else:
            trial_data.actual_response = "Unknown Response"

        client.adjustSourceLevel(source_id, 0)

        for speak in used_speakers:
            client.setLEDs(speak, '0,0,0,0')
        used_speakers = list()
        trials.append(trial_data)
    filename = str(subject_number) + ".csv"
    file = open(data_collection_folder + filename, 'w')
    line = "Trial_Number,Correct_Response,Actual_Response,Response_Time\n"
    file.write(line)
    for i, trial in enumerate(trials):
        line = str(i) + "," + str(trial.correct_response) + "," + str(trial.actual_response) + "," + \
               str(trial.response_time) + "\n"
        file.write(line)
    file.close()
    client.reset()

main()
