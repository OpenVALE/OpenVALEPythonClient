"""
***THIS TEST SCRIPT TESTS *ONLY* THE CLIENT FUNCTIONS IMPLEMENTED SO FAR***

FUNCTIONS MISSING IN CLIENT INCLUDE:
-->getCurrentHRTF
-->getSubjectNumber

FUNCTIONS MISSING IN TEST SCRIPT INCLUDE:
-->ledLightingTest
-->speakerHighlightingTest
-->waitForRecenterTest
-->getSubjectNumberTest

***SEE JAVA CLIENT FOR COMPLETED CLIENT AND TEST SCRIPT***
"""

from OpenVALEClient import *
from OpenVALEClient import LocalizationResponse

# Time to suspend program to display a message
PAUSETIME = 3


def main():
    ##################
    # --BASIC SETUP--#
    ##################

    # Set up the OV Client
    client = OpenVALEClient("localhost", 43201, True)

    # Load in the HRTF
    hrtfNo = client.loadHRTF("GoldenClusterMean_Snow_SH6E150.slh")

    # Add an audio source
    location = [1.0, 2.0, 3.0]
    noiseID = client.addAudioSource(NoiseType.noise, location, hrtfNo)
    wavID = client.addAudioSource(NoiseType.wav, location, hrtfNo, "soundTone.wav")

    # Initiate testing
    client.displayMessage("--FIRE TO BEGIN TEST SCRIPT--")
    client.waitForResponse()

    # Define front of sim
    client.defineFront()

    # Start audio rendering
    client.startRendering()

    # Keep audio sources from playing immediately
    client.muteAudioSource(noiseID, True)
    client.muteAudioSource(wavID, True)

    ############################################
    # --TESTING BEGINNING WITH A NOISE SOURCE--#
    ############################################

    client.displayMessage("AUDIO TESTING--NOISE SOURCE")
    sleep(PAUSETIME)

    # Test audio playback
    basicAudioTest(client, noiseID)

    # Test volume control
    volumeTest(client, noiseID)

    # Test setHRTF
    setHRTFTest(client, noiseID)

    ##########################################
    # --TESTING BEGINNING WITH A WAV SOURCE--#
    ##########################################

    client.displayMessage("AUDIO TESTING--WAV SOURCE")
    sleep(PAUSETIME)

    # Test audio playback
    basicAudioTest(client, wavID)

    # Test volume control
    volumeTest(client, wavID)

    # Test setHRTF
    setHRTFTest(client, wavID)

    ##########################################
    # --TESTING USER INTERACTION IN ALF SIM--#
    ##########################################

    client.displayMessage("INTERACTION TESTING")
    sleep(PAUSETIME)

    # Test of basic user interaction
    basicInteractionTest(client)

    # Test of location information methods
    locationInformationTest(client)

    # Test of LED lighting in ALF
    ledLightingTest(client, noiseID, location)

    # Test of speaker highlighting in ALF
    speakerHighlightingTest(client, noiseID, location)

    # Test of waitForRecenter-based interaction
    waitForRecenterTest(client, noiseID, location)

    # Test of getSubjectNumber
    getSubjectNumberTest(client)

    #####################
    # --END THE SCRIPT--#
    #####################

    client.displayMessage("--TEST SCRIPT COMPLETE--")

    # Reset the sim
    client.reset()

    client.displayMessage("")


def basicAudioTest(client, sourceID):
    client.displayMessage("TESTING BASIC AUDIO PLAYBACK")

    # Enable audio source
    client.enableSrc(sourceID, True)
    client.muteAudioSource(sourceID, False)
    sleep(PAUSETIME)

    # Mute audio source
    client.muteAudioSource(sourceID, True)
    client.enableSrc(sourceID, False)

    # Ensure audio muted
    sleep(PAUSETIME)

    client.displayMessage("BASIC AUDIO PLAYBACK TEST COMPLETE")
    sleep(PAUSETIME)


def volumeTest(client, sourceID):
    client.displayMessage("TESTING VOLUME CONTROL")
    sleep(PAUSETIME)

    # Enable audio
    client.enableSrc(sourceID, True)
    client.muteAudioSource(sourceID, False)

    client.displayMessage("AdjustOverallLevel")
    sleep(PAUSETIME)

    # Test decreasing volume
    i = 0
    while i >= -60:
        client.displayMessage("Volume Changed " + str(i) + " dB")
        client.adjustOverallLevel(i)
        sleep(2)
        i -= 20

    # Test increasing volume
    i = -40
    while i <= 0:
        client.displayMessage("Volume Changed " + str(i) + " dB")
        client.adjustOverallLevel(i)
        sleep(2)
        i += 20

    client.displayMessage("AdjustSourceLevel")
    sleep(PAUSETIME)

    # Set audio source to fixed position
    client.adjustSourcePosition(sourceID, 10)

    # Test decreasing volume
    i = 0
    while i >= -60:
        client.displayMessage("Volume Changed " + str(i) + " dB")
        client.adjustSourceLevel(sourceID, i)
        sleep(2)
        i -= 20

    # Test increasing volume
    i = -40
    while i <= 0:
        client.displayMessage("Volume Changed " + str(i) + " dB")
        client.adjustSourceLevel(sourceID, i)
        sleep(2)
        i += 20

    # Disable audio
    client.muteAudioSource(sourceID, True)
    client.enableSrc(sourceID, False)

    client.displayMessage("VOLUME CONTROL TESTING COMPLETE")
    sleep(PAUSETIME)


def setHRTFTest(client, sourceID):
    client.displayMessage("SETHRTF TESTING")
    sleep(PAUSETIME)

    # Start playing sound
    client.enableSrc(sourceID, True)
    client.muteAudioSource(sourceID, False)
    client.displayMessage("Using HRTF: GoldenClusterMean_Snow_SH6E150.slh")
    sleep(PAUSETIME)

    # Disable sound, switch HRTF
    client.enableSrc(sourceID, False)
    client.muteAudioSource(sourceID, True)
    tempHRTF = client.loadHRTF("GoldenClusterMean_SH6E100_HD280.slh")
    client.setHRTF(sourceID, tempHRTF)

    # Play sound with new HRTF
    client.enableSrc(sourceID, True)
    client.muteAudioSource(sourceID, False)
    client.displayMessage("Using HRTF: GoldenClusterMean_SH6E100_HD280.slh")
    sleep(PAUSETIME)

    # Revert to original HRTF
    client.enableSrc(sourceID, True)
    client.muteAudioSource(sourceID, False)
    tempHRTF = client.loadHRTF("GoldenClusterMean_Snow_SH6E150.slh")
    client.setHRTF(sourceID, tempHRTF)

    # Play sound with original HRTF
    client.enableSrc(sourceID, True)
    client.muteAudioSource(sourceID, False)
    client.displayMessage("Using HRTF: GoldenClusterMean_Snow_SH6E150.slh")
    sleep(PAUSETIME)

    # End sound playback
    client.muteAudioSource(sourceID, True)
    client.enableSrc(sourceID, False)

    client.displayMessage("SETHRTF TESTING COMPLETE")
    sleep(PAUSETIME)


def basicInteractionTest(client):
    client.displayMessage("TESTING BASIC USER INTERACTION")
    sleep(PAUSETIME)

    # Ensure free cursor, hand mode attaches to hand
    client.showFreeCursor("hand", True)
    client.displayMessage("Ensure free cursor follows hand. Fire when done.")
    client.waitForResponse()

    # Recenter
    client.defineFront()

    # Ensure free cursor, head mode attaches to head
    client.showFreeCursor("hand", False)
    client.showFreeCursor("head", True)
    client.displayMessage("Ensure free cursor follows head. Fire when done.")
    client.waitForResponse()

    # Recenter
    client.defineFront()

    # Ensure snapped cursor, hand mode attaches to hand
    client.showFreeCursor("head", False)
    client.showSnappedCursor("hand", True)
    client.displayMessage("Ensure snapped cursor follows hand. Fire when done.")
    client.waitForResponse()

    # Recenter
    client.defineFront()

    # Ensure snapped cursor, head mode attaches to head
    client.showSnappedCursor("hand", False)
    client.showSnappedCursor("head", True)
    client.displayMessage("Ensure snapped cursor follows head. Fire when done.")
    client.waitForResponse()

    # Recenter
    client.defineFront()
    client.showSnappedCursor("head", False)

    client.displayMessage("BASIC USER INTERACTION TEST COMPLETE")
    sleep(PAUSETIME)


def locationInformationTest(client):
    client.displayMessage("TEST OF LOCATION INFORMATION METHODS")
    sleep(PAUSETIME)

    # Show free cursor for navigation
    client.defineFront()
    client.showFreeCursor("head", True)

    # Tests head orientation is returned
    client.displayMessage("Fire to see current head coordinates")
    client.waitForResponse()
    client.displayMessage(str(client.getHeadOrientation()).replace(",", "") + "...Fire to continue...")
    client.waitForResponse()

    # Retest head orientation after head moves
    client.displayMessage("Move head and fire to see new head coordinates")
    client.waitForResponse()
    client.displayMessage(str(client.getHeadOrientation()).replace(",", "") + "...Fire to continue...")
    client.waitForResponse()

    # Tests 6DOF is returned
    client.displayMessage("Fire to see current 6DOF")
    client.waitForResponse()
    client.displayMessage(str(client.getHead6DOF()).replace(",", "") + "...Fire to continue...")
    client.waitForResponse()

    # Retest 6DOF after head moves
    client.displayMessage("Move head and fire to see new 6DOF")
    client.waitForResponse()
    client.displayMessage(str(client.getHead6DOF()).replace(",", "") + "...Fire to continue...")
    client.waitForResponse()

    # Switch cursor
    client.showFreeCursor("head", False)
    client.defineFront()
    client.showFreeCursor("hand", True)

    # Get position of the speaker nearest to one clicked
    client.displayMessage("Fire to see coordinates of speaker nearest to the cursor")
    response = client.waitForResponse()
    client.displayMessage(
        str(client.getSpeakerPosition(response.speakerID)).replace(",", "") + "...Fire to continue...")
    client.waitForResponse()

    # Get position of new speaker nearest to one clicked
    client.displayMessage("Move over new speaker and fire to see its coordinates")
    response = client.waitForResponse()
    client.displayMessage(
        str(client.getSpeakerPosition(response.speakerID)).replace(",", "") + "...Fire to continue...")
    client.waitForResponse()

    # Test that nearest speaker to cursor is returned
    client.displayMessage("Fire to see number of speaker nearest to the cursor")
    response = client.waitForResponse()
    client.displayMessage(
        str(client.getNearestSpeaker(client.getSpeakerPosition(response.speakerID))) + "...Fire to continue...")
    client.waitForResponse()

    # Hide cursor before further tests
    client.showFreeCursor("head", False)

    client.displayMessage("TEST OF LOCATION INFORMATION METHODS COMPLETE")
    sleep(PAUSETIME)


def ledLightingTest(client, sourceID, location):
    pass


def speakerHighlightingTest(client, sourceID, location):
    pass


def waitForRecenterTest(client, sourceID, location):
    pass


def getSubjectNumberTest(client):
    pass


# Call main function above
main()
